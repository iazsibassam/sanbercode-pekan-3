<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcome', [AuthController::class, 'welcome']);
Route::get('/register', [AuthController::class, 'regist']);
Route::get('/', [HomeController::class, 'home']);
Route::post('/kirim', [HomeController::class, 'kirim']);
Route::get('/master',function(){ return view('layout.master');});
Route::get('/table',function(){ return view('halaman.table');});
Route::get('/data-tables',function(){ return view('halaman.data-tables');});
Route::get('/cast', [CastController::class, 'index']);
Route::get('/cast/create', [CastController::class,'create']);
Route::post('/cast', [CastController::class, 'store']);
Route::get('/cast/{cast_id}', [CastController::class, 'show']);
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
Route::put('/cast/{cast_id}', [CastController::class, 'update']);
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);

