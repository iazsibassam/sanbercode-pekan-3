@extends('layout.master')
@section('judul')
Halaman Edit CAST  
@endsection
@section('judul2')
Edit Cast
@endsection
@section('content')
<h2>Edit Data</h2>
        <form action="/cast/{{$cast->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" id="nama" placeholder="Masukkan Nama">
            </div>   @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            
            <div class="form-group">
                <label for="umur">Umur</label>
                <input type="text" class="form-control" name="umur" value="{{$cast->umur}}" id="umur" placeholder="Masukkan Umur">
            </div>   @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            
            <div class="form-group">
                <label for="bio">Bio</label>
                <input type="text" class="form-control" name="bio" value="{{$cast->bio}}" id="bio" placeholder="Masukkan Bio">
            </div>  @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            
            <button type="submit" class="btn btn-primary">Simpan</button>
        </form>@endsection