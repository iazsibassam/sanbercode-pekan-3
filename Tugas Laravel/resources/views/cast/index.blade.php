@extends('layout.master')
@section('judul')
CAST
@endsection
@section('judul2')
list Cast
@endsection
@section('content')
<a href="/cast/create" class="btn btn-primary btn-sm mb-3">Tambah Data</a>
<table class="table">
    <thead>
    <tr>
    <th scope="col">#</th>
    <th scope="col">Nama</th>
    <th scope="col">umur</th>
    <th scope="col">Action</th>
    </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $value )
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$value->nama}}</td>
            <td>{{$value->umur}}</td>
            <td><Form Action="/cast/{{$value->id}}" method="POST">@csrf
                <a href="/cast/{{$value->id}}" class="btn btn-info btn-sm">Detail</a>
                <a href="/cast/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                @method('DELETE')<input type="submit" value="delete" class="btn btn-danger btn-sm"></form>
            </td>
        </tr>
    @empty
    <p>Data Tidak Ada</p>
@endforelse

</tbody>
</table>

@endsection