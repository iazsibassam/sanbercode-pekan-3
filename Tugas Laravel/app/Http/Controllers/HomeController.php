<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home()
    {
        return view('halaman.home');
    }

    public function kirim(request $request)
    {
       $first = $request['first'];
       $last = $request['last'];
       return view ('halaman.welcome',['first' => $first, 'last'=> $last]);
    }
}
