<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function welcome()
    {
        return view('halaman.welcome');
    }
    public function regist()
    {
        return view('halaman.register');
    }
    public function table()
    {
        return view('halaman.table');
    }
    public function master()
    {
        return view('layout.master');
    }
}
